# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import functools
import logging

import json
import random
import traceback

import werkzeug.urls
import werkzeug.utils
from werkzeug.exceptions import BadRequest

from odoo import api, http, SUPERUSER_ID, _
from odoo.exceptions import AccessDenied
from odoo.http import request
from odoo import registry as registry_get

from odoo.addons.web.controllers.main import Home, Session
from odoo.addons.web.controllers.main import db_monodb, ensure_db, set_cookie_and_redirect, login_and_redirect


_logger = logging.getLogger(__name__)


#----------------------------------------------------------
# helpers
#----------------------------------------------------------
def fragment_to_query_string(func):
    @functools.wraps(func)
    def wrapper(self, *a, **kw):
        kw.pop('debug', False)
        if not kw:
            return """<html><head><script>
                var l = window.location;
                var q = l.hash.substring(1);
                var r = l.pathname + l.search;
                if(q.length !== 0) {
                    var s = l.search ? (l.search === '?' ? '' : '&') : '?';
                    r = l.pathname + l.search + s + q;
                }
                if (r == l.pathname) {
                    r = '/';
                }
                window.location = r;
            </script></head><body></body></html>"""
        return func(self, *a, **kw)
    return wrapper


#----------------------------------------------------------
# Controller
#----------------------------------------------------------
class OAuthLogin(Home):
    def generate_nonce(self,length=8):
        return ''.join([str(random.randint(0, 9)) for i in range(length)])

    def list_providers(self):
        _logger.info('list_providers ---------')
        try:
            providers = request.env['auth.oidc.provider'].sudo().search_read([])
            _logger.info(providers)
        except Exception:
            providers = []
            _logger.info('no providers')
        for provider in providers:
            _logger.info('--------test 3-------------')
            return_url = request.httprequest.url_root + 'auth_oauth/signin'
            state = self.get_state(provider)
            params = dict(
                response_type='token',
                client_id=provider['client_id'],
                redirect_uri=return_url,
                scope=provider['scope'],
                state=json.dumps(state),
                nonce=self.generate_nonce(),
            )
            provider['auth_link'] = "%s?%s" % (provider['authorization_endpoint'], werkzeug.url_encode(params))
            _logger.info(provider)
        _logger.info(providers)
        return providers

    def get_state(self, provider):
        redirect = request.params.get('redirect') or 'web'
        if not redirect.startswith(('//', 'http://', 'https://')):
            redirect = '%s%s' % (request.httprequest.url_root, redirect[1:] if redirect[0] == '/' else redirect)
        state = dict(
            d=request.session.db,
            p=provider['id'],
            r=werkzeug.url_quote_plus(redirect),
        )
        token = request.params.get('token')
        if token:
            state['t'] = token
        return state

    @http.route()
    def web_login(self, *args, **kw):
        _logger.info('--------test 0-------------')
        ensure_db()
        if request.httprequest.method == 'GET' and request.session.uid and request.params.get('redirect'):
            # Redirect if already logged in and redirect param is present
            return http.redirect_with_hash(request.params.get('redirect'))
        providers = self.list_providers()
        _logger.info('--------test 1-------------')
        if len(providers) == 1:
            return http.redirect_with_hash(providers[0]['auth_link'])
        _logger.info('--------test 2-------------')
        response = super(OAuthLogin, self).web_login(*args, **kw)
        if response.is_qweb:
            error = request.params.get('oauth_error')
            if error == '1':
                error = _("Sign up is not allowed on this database.")
            elif error == '2':
                error = _("Access Denied")
            elif error == '3':
                error = _("You do not have access to this database or your invitation has expired. Please ask for an invitation and be sure to follow the link in your invitation email.")
            else:
                error = None

            response.qcontext['providers'] = providers
            if error:
                response.qcontext['error'] = error

        return response

class OAuthSession(Session):
    @http.route()
    def logout(self, redirect='/web'):
        _logger.info('--------test 5-------------')
        providers = request.env['auth.oidc.provider'].sudo().search_read([])
        if len(providers) == 1:
            request.session.logout(keep_db=True)
            params = dict(redirect_uri='%s%s' % (request.httprequest.url_root, redirect))
            return http.redirect_with_hash('%s?%s' % (providers[0]['end_session_endpoint'], werkzeug.url_encode(params)))

        return super(OAuthSession, self).logout(redirect)

class OAuthController(http.Controller):
    @http.route('/auth_oauth/signin', type='http', auth='none')
    @fragment_to_query_string
    def signin(self, **kw):
        _logger.info("---next 0---")
        state = json.loads(kw['state'])
        dbname = state['d']
        provider = state['p']
        context = state.get('c', {})
        registry = registry_get(dbname)
        with registry.cursor() as cr:
            try:
                env = api.Environment(cr, SUPERUSER_ID, context)
                _logger.info("---next 1---")
                credentials = env['res.users'].sudo().auth_oauth(provider, kw)
                _logger.info("---next 2---")
                cr.commit()
                _logger.info(credentials)
                action = state.get('a')
                menu = state.get('m')
                redirect = werkzeug.url_unquote_plus(state['r']) if state.get('r') else False
                url = '/web'
                if redirect:
                    url = redirect
                elif action:
                    url = '/web#action=%s' % action
                elif menu:
                    url = '/web#menu_id=%s' % menu
                _logger.info("---next 3---")
                resp = login_and_redirect(*credentials, redirect_url=url)
                _logger.info("---next 4---")
                # Since /web is hardcoded, verify user has right to land on it
                if werkzeug.urls.url_parse(resp.location).path == '/web' and not request.env.user.has_group('base.group_user'):
                    resp.location = '/'
                return resp
            except AccessDenied:
                # oauth credentials not valid, user could be on a temporary session
                _logger.info('OAuth2: access denied, redirect to main page in case a valid session exists, without setting cookies')
                url = "/web/login?oauth_error=3"
                redirect = werkzeug.utils.redirect(url, 303)
                redirect.autocorrect_location_header = False
                return redirect
            except Exception as e:
                # signup error
                _logger.exception("OAuth2: [Exception] %s" % str(e))
                url = "/web/login?oauth_error=2"

        return set_cookie_and_redirect(url)
