# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'OpenID Connect Authentication',
    'category': 'Tools',
    'description': """
Allow users to login through OpenID Connect Provider.
=============================================
""",
    'author': "eHealth ID",
    'website': "https://ehealth.id",

    'category': 'Uncategorized',
    'version': '0.1',

    'maintainer': 'eHealth ID',
    'depends': ['base', 'web', 'base_setup'],
    'data': [
        'views/auth_oauth_views.xml',
        'views/auth_oauth_templates.xml',
        'views/res_users_views.xml',
    ],
    'external_dependencies': {
        'python': ['jwt'],
    },
}
