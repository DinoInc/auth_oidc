# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
import requests

from odoo import api, fields, models
from odoo.exceptions import except_orm

from odoo.addons import base
base.res.res_users.USER_PRIVATE_FIELDS.append('oidc_access_token')

_logger = logging.getLogger(__name__)

class AuthOProvider(models.Model):
    _name = 'auth.oidc.provider'
    _description = 'OpenID Connect provider'
    _order = 'name'

    name = fields.Char(string='Provider name', required=True)
    client_id = fields.Char(string='Client ID', required=True)
    scope = fields.Char(default='openid')
    endpoint = fields.Char(string='OpenID Connect Endpoint', required=True)

    authorization_endpoint = fields.Char(compute='_get_endpoints', store=True)
    userinfo_endpoint = fields.Char(compute='_get_endpoints', store=True)
    end_session_endpoint = fields.Char(compute='_get_endpoints', store=True)

    def _get_params(self):
        try:
            discovery_url = self.endpoint + "/.well-known/openid-configuration"
            return requests.get(discovery_url).json()
        except:
            raise except_orm("OpenID Connect discovery URL not found",
                "OpenID Connect discovery URL not found ({}), please ensure it's valid OpenID Connect endpoint.".format(discovery_url))

    @api.depends('endpoint')
    def _get_endpoints(self):
        params = self._get_params() 
        self.authorization_endpoint = params['authorization_endpoint']
        self.userinfo_endpoint = params['userinfo_endpoint']
        self.end_session_endpoint = params['end_session_endpoint']
