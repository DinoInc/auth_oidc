# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import json

import logging
import requests

from odoo import api, fields, models
from odoo.exceptions import AccessDenied, UserError
from odoo.addons.auth_signup.models.res_users import SignupError

from odoo.addons import base
base.res.res_users.USER_PRIVATE_FIELDS.append('oidc_access_token')

_logger = logging.getLogger(__name__)

class ResUsers(models.Model):
    _inherit = 'res.users'

    oidc_provider_id = fields.Many2one('auth.oidc.provider', string='OpenID Connect Provider')
    oidc_sub = fields.Char(string='Subject', help="OpenID Connect subject (sub)", copy=False)
    oidc_access_token = fields.Char(string='Access Token', readonly=True, copy=False)

    _sql_constraints = [
        ('uniq_users_oidc_provider_oidc_uid', 'unique(oidc_provider_id, oidc_sub)', 'OpenID Connect subject (sub) must be unique per provider'),
    ]


    @api.model
    def _auth_oauth_rpc(self, endpoint, access_token):
        response = requests.get(endpoint, headers={"Authorization":"bearer " + access_token}, params={'access_token': access_token})
        _logger.info(access_token);
        _logger.info(endpoint)
        _logger.info(response)
        _logger.info(response.json())
        return response.json()

    @api.model
    def _auth_oauth_validate(self, provider, access_token):
        """ return the validation data corresponding to the access token """
        oauth_provider = self.env['auth.oidc.provider'].browse(provider)
        validation = self._auth_oauth_rpc(oauth_provider.userinfo_endpoint, access_token)
        if validation.get("error"):
            raise Exception(validation['error'])
        return validation

    @api.model
    def _auth_oauth_signin(self, provider, validation, params):
        """ retrieve and sign in the user corresponding to provider and validated access token
            :param provider: oauth provider id (int)
            :param validation: result of validation of access token (dict)
            :param params: oauth parameters (dict)
            :return: user login (str)
            :raise: AccessDenied if signin failed

            This method can be overridden to add alternative signin methods.
        """
        subject = validation['sub']
        try:
            oauth_user = self.search([("oidc_sub", "=", subject), ('oidc_provider_id', '=', provider)])
            if not oauth_user:
                raise AccessDenied()
            assert len(oauth_user) == 1
            oauth_user.write({'oidc_access_token': params['access_token']})
            return oauth_user.login
        except AccessDenied as access_denied_exception:
            if self.env.context.get('no_user_creation'):
                return None
            state = json.loads(params['state'])
            token = state.get('t')
            values = self._generate_signup_values(provider, validation, params)
            try:
                _, login, _ = self.signup(values, token)
                return login
            except (SignupError, UserError):
                raise access_denied_exception

    @api.model
    def auth_oauth(self, provider, params):
        # Advice by Google (to avoid Confused Deputy Problem)
        # if validation.audience != OUR_CLIENT_ID:
        #   abort()
        # else:
        #   continue with the process
        access_token = params.get('access_token')
        validation = self._auth_oauth_validate(provider, access_token)
        # retrieve and sign in user
        login = self._auth_oauth_signin(provider, validation, params)
        if not login:
            raise AccessDenied()
        # return user credentials
        return (self.env.cr.dbname, login, access_token)

    @api.model
    def check_credentials(self, password):
        try:
            return super(ResUsers, self).check_credentials(password)
        except AccessDenied:
            res = self.sudo().search([('id', '=', self.env.uid), ('oidc_access_token', '=', password)])
            if not res:
                raise
